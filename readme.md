# Initialize Ubuntu OS

If you don't have access to a flash drive, you can download this code using a browser:
    https://bitbucket.org/travishudson/init-ubuntu/downloads/

TODO: Do you not have to do the private key setup if you clone via https://travishudson@bitbucket.org/travishudson/init-ubuntu.git ?
Or download this code by setting up git and doing a clone:
    sudo apt-get install git-all

    Generate a key pair:
        cd $HOME
        ssh-keygen
        (enter)
        (enter)
        (enter)

    Log in to bitbucket.org, to the account with this repository:
        In avatar > Bitbucket settings > Security > SSH keys
            Add the output of:
                cat ~/.ssh/id_rsa.pub

    cd ~/Desktop
    git clone git@bitbucket.org:travishudson/init-ubuntu.git
    cd init-ubuntu

Run these commands in this project's root folder to initialize a fresh installation of Ubuntu:
    sudo su -
    apt-get update
    apt-get install python3.6
    Get rid of the symlink poiting to (/etc/python3.6/sitecustomize.py) that causes an error (https://bugs.launchpad.net/ubuntu/+source/python3.6/+bug/1631367): 
        sudo rm /usr/lib/python3.6/sitecustomize.py
    python3.6 init.py
    exit

