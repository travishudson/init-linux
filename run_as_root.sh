#!/bin/bash

# packages to be installed
gitversion="v2.12.2"
gitversion_str="git version 2.12.2"    # what "git --version" will output after the right version is installed
gopackage="go1.8.1.linux-amd64.tar.gz"
goversion_str="go version go1.8.1 linux/amd64"   # what "go version" will output after the right version is installed
declare -a packages=(
    "git"          # version control
    "vim"          # vi improved
    "vim-gnome"    # vim with a gui
    "python3.6"    # advanced python3 interpreter
    "vlc"          # video player
    "chromium-browser" # chrome browser
    "tmux"         # terminal multiplexer; can attach/detach terminal sessions at will
    "hwloc"        # provides lstopo, which shows a hardware summary
    "htop"         # shows current processor usage
    "checkinstall" # replacement for "sudo make install"; makes it easier to uninstall
    "nodejs"       # node.js
    "mumble"       # voice over IP application
    "calibre"      # ebook reader
)
# optional packages available with apt-get:
    # "steam"        # steam video game distribution platform
    # "playonlinux"  # allows for playing league of legends

# TODO: Install wine using apt-get?
# TODO: Install ScudCloud? https://itsfoss.com/slack-use-linux/

# error out if this script is not being run as root
if [[ `id -u` -ne 0 ]]; then
    (>&2 echo "Error: This script must be run as root.")
    exit 1
fi

# TODO: There's no reason to explicitly pass in a username if we're doing this check. Just get the username from some environment variable, such as $HOME.
# error out if sudo was not used to get root (we need $HOME to still make sense, not be "/root")
if [ "$HOME" = "/root" ]; then
    (>&2 echo "Error: This script must be run as root via 'sudo ./script.sh', not by using 'sudo su -' then running './script.sh'.")
    exit 1
fi

# error out if no username was supplied as a parameter on the command line
if [[ -z "$1" ]]; then
    (>&2 echo "Error: Must pass in a username as a parameter.")
    exit 1
fi
username="$1"

# check to make sure the given username is valid
su "${username}" -c "true"
if [[ $? -ne 0 ]]; then
    (>&2 echo "Error: Could not run a command as the user: ${username}")
    exit 1
fi

# error out if running on macOS
osname=`uname`
if [ "$osname" = "Darwin" ]; then
    (>&2 echo "Error: This script cannot run on macOS.")
    exit 1
fi

# require an internet connection to continue
echo "Checking for an internet connection..."
wget -q --tries=10 --timeout=20 --spider http://google.com
if [[ $? -ne 0 ]]; then
    (>&2 echo "Error: No internet connection.")
    exit 1
fi

# prevent the screen from locking while this script is running
if hash gnome-shell 2>/dev/null; then
    echo "Gnome detected. Preventing screen lock."
    su "${username}" -c "gsettings set org.gnome.desktop.session idle-delay 0"
fi

# initialize .bash_profile
if [ -f "$HOME/.bash_profile" ]; then
    echo "~/.bash_profile already exists. Choosing to not overwrite it."
else
    echo "Initializing ~/.bash_profile..."
    cp bash_profile.sh $HOME/.bash_profile
fi

# append customizations to .bashrc if it seems like the customizations have not already been added
if grep -q "BEGIN USER CUSTOMIZATIONS" $HOME/.bashrc; then
    echo "~/.bashrc already has customizations appended. Skipping this section."
else
    echo "Appending customizations to ~/.bashrc..."
    cat $HOME/.bashrc bashrc.sh > tmp_bashrc
    mv tmp_bashrc $HOME/.bashrc
fi

# initialize .vimrc
if [ -f "$HOME/.vimrc" ]; then
    echo "~/.vimrc already exists. Choosing to not overwrite it."
else
    echo "Initializing ~/.vimrc..."
    cp vimrc.txt $HOME/.vimrc
fi

# TODO: Enable tmux-resurrect in the configuration file: run-shell ~/tmux/tmux-resurrect/resurrect.tmux
# initialize .tmux.conf
if [ -f "$HOME/.tmux.conf" ]; then
    echo "~/.tmux.conf already exists. Choosing to not overwrite it."
else
    echo "Initializing ~/.tmux.conf..."
    cp tmux-conf.txt $HOME/.tmux.conf
fi

# set up node.js repository
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -

# add the repository that contains the latest version of mumble
add-apt-repository ppa:mumble/release

# install many useful packages with apt-get
if hash apt-get 2>/dev/null; then
    echo "Installing apt-get packages: ${packages[@]}"
    apt-get update
    for i in "${packages[@]}"
    do
        apt-get -y -q --no-upgrade install "$i"
    done
    apt-get autoremove
else
    echo "WARNING: apt-get not found. The following packages were not installed: ${packages[@]}"
fi

# update the node.js package manager
npm install npm --global

# install the latest git package for linux
current_git_version=`git --version`
if [ "$current_git_version" = "$gitversion_str" ]; then
    echo "git is already up-to-date. Skipping installation of git from source."
else
    echo "Installing git from source..."
    if hash apt-get 2>/dev/null; then

        # install packages git needs for building from source
        apt-get update
        apt-get -y -q install libcurl4-gnutls-dev libexpat1-dev gettext libz-dev libssl-dev asciidoc xmlto docbook2x

        if [[ $? -eq 0 ]]; then

            # use the current version of git to pull down the git repo
            git clone https://git.kernel.org/pub/scm/git/git.git
            if [[ $? -eq 0 ]]; then
                cd git
                git checkout tags/${gitversion}
                make all doc info prefix=/usr
                make install install-doc install-html install-info install-man prefix=/usr
                cd ..
                rm -rf git
            else
                echo "WARNING: Problem cloning git repo. Skipping installation of git from source."
            fi

        else
            echo "WARNING: Problem installing dependencies needed to build git. Skipping installation of git from source."
        fi

        apt-get autoremove
    else
        echo "WARNING: apt-get not found. Skipping installation of git from source."
    fi
fi

# configure git
su "${username}" -c "git config alias.st &>/dev/null"
if [[ $? -eq 0 ]]; then
    echo "'git st' is already set, not overwriting"
else
    echo "setting the 'git st' alias"
    su "${username}" -c "git config --global alias.st 'status -sb'"
fi

# install the latest golang package for linux
current_go_version=`go version`
if [ "$current_go_version" = "$goversion_str" ]; then
    echo "go is already up-to-date. Skipping installation of go from source."
else
    echo "Installing go from source..."
    wget https://storage.googleapis.com/golang/${gopackage}
    if [[ $? -eq 0 ]]; then
        tar -C /usr/local -xzf ${gopackage}
        rm -f ${gopackage}
    else
        echo "WARNING: Skipping installation of go because of a wget error."
    fi
fi

# fix the "system program problem detected" error caused by apport
if [ -f "/etc/default/apport" ]; then
    echo "Turning off apport..."
    rm -f /var/crash/*
    sed -i -e "s/enabled=1/enabled=0/g" /etc/default/apport
fi

# give gnome3 maximize and minimize buttons
if hash gnome-shell 2>/dev/null; then
    echo "Gnome detected. Configuring GUI..."
    su "${username}" -c "gsettings set org.gnome.desktop.wm.preferences button-layout 'close,minimize,maximize:'"

    # Settings > Power > Power Saving
    su "${username}" -c "gsettings set org.gnome.desktop.session idle-delay 300"  # lock the screen after 5 minutes
fi

