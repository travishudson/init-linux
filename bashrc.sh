
# BEGIN USER CUSTOMIZATIONS

# Configure golang environment variables
export GOROOT=/usr/local/go
export GOPATH=/home/travis

# Make it easy to cd to golang projects
export CDPATH=$CDPATH:$GOPATH/src/bitbucket.org/travishudson

# Configure $PATH
export PATH=$GOROOT/bin:$PATH

